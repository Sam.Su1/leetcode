from typing import List

class Evaluate_Reverse_Polish_Notation:
    """https://leetcode.com/problems/evaluate-reverse-polish-notation/
    Evaluate the value of an arithmetic expression in Reverse Polish Notation.

    Valid operators are +, -, *, and /.
    Each operand may be an integer or another expression.

    Note that division between two integers should truncate toward zero.

    It is guaranteed that the given RPN expression is always valid. 
    That means the expression would always evaluate to a result, 
    and there will not be any division by zero operation.

    Constraint:
    1 <= tokens.length <= 104
    tokens[i] is either an operator: "+", "-", "*", or "/", 
    or an integer in the range [-200, 200].
    """
    def op(self, x, y, operator):
        # cannot use dictionary here, since all cases will be evaluated
        # and may lead to divide by 0 if one of x, y is 0
        if operator == '+':
            return x+y
        elif operator == '-':
            return x-y
        elif operator == '*':
            return x*y
        else:
            return int(x/y)

    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        for token in tokens:
            if token.isnumeric() or \
                (token.startswith('-') and token[1:].isnumeric()):
                stack.append(int(token))
            else:
                y = stack.pop()
                x = stack.pop()
                stack.append(self.op(x, y, token))
        return stack.pop()
