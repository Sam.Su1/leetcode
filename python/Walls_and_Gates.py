from typing import List

class Walls_and_Gates:
    """https://leetcode.com/problems/walls-and-gates/
    You are given an m x n grid rooms with these three possible values.
    -1  A wall or an obstacle
    0   A gate.
    INF Infinity means an empty room. We use the value 2^31-1=2147483647 to
    represent INF as you may assume that the distance to a gate is less than 2147483647.

    Fill each empty room with the distance to its nearest gate. If it is
    impossible to reach a gate, it should be filled with INF.

    Constraints:
    m == rooms.length
    n == rooms[i].length
    1 <= m, n <= 250
    rooms[i][j] is -1, 0, or 2^31-1
    """
    def get_neighbors(self, rooms, r, c):
        # return the list of neighbors of a cell
        # Left, Down, Right, Up
        neighbors = []
        if c > 0 and rooms[r][c-1] != -1:
            neighbors.append((r, c-1))
        if r+1 < len(rooms) and rooms[r+1][c] != -1:
            neighbors.append((r+1, c))
        if c+1 < len(rooms[r]) and rooms[r][c+1] != -1:
            neighbors.append((r, c+1))
        if r > 0 and rooms[r-1][c] != -1:
            neighbors.append((r-1, c))
        return neighbors

    def BFS(self, rooms, q):
        # simultaneous BFS from all pairs of indices 
        # within the queue
        while q:
            i, j = q.pop(0)
            print(i, j)
            for neighbor in self.get_neighbors(rooms, i, j):
                n_i, n_j = neighbor
                if rooms[n_i][n_j] == 2147483647:
                    # reached empty space, update distance
                    rooms[n_i][n_j] = rooms[i][j] + 1
                    q.append(neighbor)
        return rooms

    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        """
        :type rooms: List[List[int]]
        :rtype: None Do not return anything, modify rooms in-place instead.
        """
        q = []
        # sweep through the rooms and obtain all 'gates'
        for r, row in enumerate(rooms):
            for c, room in enumerate(row):
                if not room:
                    q.append((r, c))
        return self.BFS(rooms, q)