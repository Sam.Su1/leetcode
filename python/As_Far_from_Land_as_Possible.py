from typing import List
import numpy as np

class As_Far_from_Land_as_Possible:
    """https://leetcode.com/problems/as-far-from-land-as-possible/
    Given an n x n grid containing only values 0 and 1, 
    where 0 represents water and 1 represents land, 
    find a water cell such that its distance to the nearest 
    land cell is maximized, and return the distance. 
    If no land or water exists in the grid, return -1.

    The distance used in this problem is the Manhattan distance: 
    the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.

    Constraints:
        n == grid.length
        n == grid[i].length
        1 <= n <= 100
        grid[i][j] is 0 or 1
    """
    def get_neighbors(self, grid, r, c):
        # return the list of neighbors of a cell
        # Left, Down, Right, Up
        neighbors = []
        if c > 0:
            neighbors.append((r, c-1))
        if r+1 < len(grid):
            neighbors.append((r+1, c))
        if c+1 < len(grid):
            neighbors.append((r, c+1))
        if r > 0:
            neighbors.append((r-1, c))
        return neighbors

    def BFS(self, grid, q) -> int:
            # BFS from source node r,c 
            dist = np.zeros_like(grid)
            maxDist = -1
            while q:
                i, j = q.pop(0)
                for neighbor in self.get_neighbors(grid, i, j):
                    n_i, n_j = neighbor
                    if not dist[n_i][n_j] and not grid[n_i][n_j]:
                        # unvisited water block, update distance to nearest
                        # land block by incrementing by one
                        dist[n_i][n_j] = dist[i][j] + 1
                        maxDist = max(maxDist, dist[n_i][n_j])
                        q.append(neighbor)
            return maxDist

    def maxDistance(self, grid: List[List[int]]) -> int:
        # BFS on each land block, simultaneously
        # we visit each water block once, counting the distance 
        # from said water block to the nearest land (guaranteed by queue in BFS)
        # we desire the largest distance to nearest land from a water block
        # roughly 2*n^2
        q = []
        for i, r in enumerate(grid):
            for j, e in enumerate(r):
                if e: # land
                    q.append((i, j))
        return self.BFS(grid, q)
