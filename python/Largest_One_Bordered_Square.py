from typing import List
class Largest_One_Bordered_Square:
    """https://leetcode.com/problems/largest-1-bordered-square/
    Given a 2D grid of 0s and 1s, return the number of elements 
    in the largest square subgrid that has all 1s on its border, 
    or 0 if such a subgrid doesn't exist in the grid.

    Constraints:
        1 <= grid.length <= 100
        1 <= grid[0].length <= 100
        grid[i][j] is 0 or 1
    """
    def largest1BorderedSquare(self, grid: List[List[int]]) -> int:
        return 0