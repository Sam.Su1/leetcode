from typing import List
import numpy as np

class Grumpy_Bookstore_Owner:
    """https://leetcode.com/problems/grumpy-bookstore-owner/
    There is a bookstore owner that has a store open for n minutes. 
    Every minute, some number of customers enter the store. 
    You are given an integer array customers of length n where 
    customers[i] is the number of the customer that enters the store at the 
    start of the ith minute and all those customers leave after the end 
    of that minute.

    On some minutes, the bookstore owner is grumpy. You are given a 
    binary array grumpy where grumpy[i] is 1 if the bookstore owner is grumpy 
    during the ith minute, and is 0 otherwise.

    When the bookstore owner is grumpy, the customers of that minute 
    are not satisfied, otherwise, they are satisfied.

    The bookstore owner knows a secret technique to keep themselves 
    not grumpy for `minutes` consecutive minutes, but can only use it once.

    Return the maximum number of customers that can be 
    satisfied throughout the day.

    Constraints:
        n == customers.length == grumpy.length
        1 <= minutes <= n <= 2 * 104
        0 <= customers[i] <= 1000
        grumpy[i] is either 0 or 1.
    """
    def maxSatisfied(self, customers: List[int], 
                     grumpy: List[int], minutes: int) -> int:
        # minimum satisfied = sum grumpy * customers 
        # maximum = minimum + `minutes` length sequence of containing most '0'
        # multiply by the sequence's customers (minus already counted)
        maxGain = 0
        for i in range(minutes):
            maxGain += customers[i] * grumpy[i]
        prevGain = maxGain # current gain for using power from [0, minutes]
        maxSat = 0 if grumpy[0] else customers[0]
        for i in range(1, len(customers)):
            if not grumpy[i]:
                maxSat += customers[i]
            # comput the gain for using power from [i, i+minutes]
            # we just need to think about the previous excluded window and
            # the next appended window
            gain = prevGain
            if grumpy[i-1]:
                gain -= customers[i-1]
            if i+minutes-1 < len(customers) and grumpy[i+minutes-1]:
                gain += customers[i+minutes-1]
            maxGain = max(maxGain, gain)
            prevGain = gain
        return maxSat + maxGain