from typing import List

class Plus_One:
    """https://leetcode.com/problems/plus-one/
    You are given a large integer represented as an integer array digits, 
    where each digits[i] is the ith digit of the integer. 
    The digits are ordered from most significant to least significant 
    in left-to-right order. The large integer does not contain any leading 0's.

    Increment the large integer by one and return the resulting array of digits.

    Constraints:
    1 <= digits.length <= 100
    0 <= digits[i] <= 9
    digits does not contain any leading 0's.
    """
    def plusOne(self, digits: List[int]) -> List[int]:
        # check the least sigificant digit, 
        # and propagate the carry
        newDigits = []
        carry = 1
        for i in range(len(digits)-1, -1, -1):
            plusOne = digits[i] + carry
            carry = 1 if plusOne >= 10 else 0
            newDigits.insert(0, plusOne % 10)
        if carry == 1: # addition resulted in a one bit larger integer
            newDigits.insert(0, carry)
        return newDigits