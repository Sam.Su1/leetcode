from string import ascii_uppercase
from functools import reduce
from operator import add

class Excel_Sheet_Column_Number:
    """https://leetcode.com/problems/excel-sheet-column-number/
    Given a string columnTitle that represents the column 
    title as appear in an Excel sheet, return its corresponding column number.

    Constraints:
        1 <= columnTitle.length <= 7
        columnTitle consists only of uppercase English letters.
        columnTitle is in the range ["A", "FXSHRXW"].
    """
    def titleToNumber(self, columnTitle: str) -> int:
        # ord('A') = ascii of A
        # similar to binary to integer conversion
        col_num = reduce(add, 
            map(lambda i_char: (pow(26, len(columnTitle) - i_char[0] - 1) * 
                               (ord(i_char[1]) - ord('A') + 1)), 
                enumerate(columnTitle)))
        return col_num
