from unittest import main, TestCase
from Excel_Sheet_Column_Number import Excel_Sheet_Column_Number
from As_Far_from_Land_as_Possible import As_Far_from_Land_as_Possible
from Grumpy_Bookstore_Owner import Grumpy_Bookstore_Owner
from Evaluate_Reverse_Polish_Notation import Evaluate_Reverse_Polish_Notation
from Largest_One_Bordered_Square import Largest_One_Bordered_Square
from Plus_One import Plus_One
from Linked_List_Cycle_II import Linked_List_Cycle_II, ListNode
from Walls_and_Gates import Walls_and_Gates

class Test_Excel_Sheet_Column_Number(TestCase):
    def setUp(self):
        self.solution = Excel_Sheet_Column_Number()

    def test_A(self):
        self.assertEqual(self.solution.titleToNumber("A"), 1)

    def test_AB(self):
        self.assertEqual(self.solution.titleToNumber("AB"), 28)

    def test_ZY(self):
        self.assertEqual(self.solution.titleToNumber("ZY"), 701)
    
    def test_FXSHRXW(self):
        self.assertEqual(self.solution.titleToNumber("FXSHRXW"), 2147483647)

class Test_As_Far_from_Land_as_Possible(TestCase):
    def setUp(self):
        self.solution = As_Far_from_Land_as_Possible()

    def test_grid1(self):
        self.assertEqual(self.solution.maxDistance([[1,0,1],[0,0,0],[1,0,1]]), 2)

    def test_grid2(self):
        self.assertEqual(self.solution.maxDistance([[1,0,0],[0,0,0],[0,0,0]]), 4)

class Test_Grumpy_Bookstore_Owner(TestCase):
    def setUp(self):
        self.solution = Grumpy_Bookstore_Owner()

    def test_grumpy1(self):
        self.assertEqual(self.solution.maxSatisfied(
            [1,0,1,2,1,1,7,5], [0,1,0,1,0,1,0,1], 3), 16)

    def test_grumpy2(self):
        self.assertEqual(self.solution.maxSatisfied([1], [0], 1), 1)

class Test_Evaluate_Reverse_Polish_Notation(TestCase):
    def setUp(self):
        self.solution = Evaluate_Reverse_Polish_Notation()

    def test_commutative_expr(self):
        self.assertEqual(self.solution.evalRPN(
            ["2","1","+","3","*"]), 9)

    def test_noncommutative_expr(self):
        self.assertEqual(self.solution.evalRPN(["4","13","5","/","+"]), 6)

    def test_negatives_and_round_toward_zero(self):
        self.assertEqual(self.solution.evalRPN(
            ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]), 22)

class Test_Largest_One_Bordered_Square(TestCase):
    def setUp(self):
        self.solution = Largest_One_Bordered_Square()

    # def test_one_b(self):
    #     self.assertEqual(self.solution.evalRPN(
    #         ["2","1","+","3","*"]), 9)

    # def test_noncommutative_expr(self):
    #     self.assertEqual(self.solution.largest1BorderedSquare(
    #         [[1,1,1],[1,0,1],[1,1,1]]), 9)

    # def test_negatives_and_round_toward_zero(self):
    #     self.assertEqual(self.solution.largest1BorderedSquare([[1,1,0,0]]), 1)

class Test_Plus_One(TestCase):
    def setUp(self):
        self.solution = Plus_One()

    def test_incre_one_last(self):
        self.assertEqual(self.solution.plusOne([1,2,3]), [1,2,4])

    def test_incre_one_prop(self):
        self.assertEqual(self.solution.plusOne([9]), [1, 0])

class Test_List_Cycle_II(TestCase):
    def setUp(self):
        self.solution = Linked_List_Cycle_II()

    def test1(self):
        node1, node2, node3, node4 = ListNode(3), ListNode(2), ListNode(0), ListNode(-4)
        node1.next = node2
        node2.next = node3
        node3.next = node4
        node4.next = node2
        self.assertEqual(self.solution.detectCycle(node1), node2)

    def test2(self):
        node1, node2 = ListNode(1), ListNode(2)
        node1.next = node2
        node2.next = node1
        self.assertEqual(self.solution.detectCycle(node1), node1)

    def test_no_cycle(self):
        node1 = ListNode(1)
        self.assertIsNone(self.solution.detectCycle(node1))

    def test_non_unique(self):
        node1, node2, node3 = ListNode(2), ListNode(2), ListNode(0)
        node1.next = node2
        node2.next = node3
        node3.next = node2
        self.assertEqual(self.solution.detectCycle(node1), node2)

class Test_Walls_and_Gates(TestCase):
    def setUp(self):
        self.solution = Walls_and_Gates()

    def test_square(self):
        rooms = [
            [2147483647,-1,0,2147483647],
            [2147483647,2147483647,2147483647,-1],
            [2147483647,-1,2147483647,-1],
            [0,-1,2147483647,2147483647]
        ]
        self.solution.wallsAndGates(rooms)
        self.assertEqual(
            rooms,
            [[3, -1, 0, 1], [2, 2, 1, -1], [1, -1, 2, -1], [0, -1, 3, 4]]
        )

    def test_edge(self):
        rooms = [[-1]]
        self.solution.wallsAndGates(rooms)
        self.assertEqual(rooms, [[-1]])

    def test_non_square(self):
        rooms = [[2147483647,0,2147483647,2147483647,0,2147483647,-1,2147483647]]
        self.solution.wallsAndGates(rooms)
        self.assertEqual(rooms, [[1, 0, 1, 1, 0, 1, -1, 2147483647]])

if __name__ == '__main__':
    main()