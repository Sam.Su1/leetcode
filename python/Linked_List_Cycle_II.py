from typing import Optional
import numpy as np

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Linked_List_Cycle_II:
    """https://leetcode.com/problems/linked-list-cycle-ii/
    Given the head of a linked list, return the node where the cycle begins. 
    If there is no cycle, return null.

    There is a cycle in a linked list if there is some node in the list 
    that can be reached again by continuously following the next pointer. 
    Internally, pos is used to denote the index of the node that tail's 
    next pointer is connected to (0-indexed). It is -1 if there is no cycle. 
    Note that pos is not passed as a parameter.

    Do not modify the linked list.

    Constraints:
        The number of the nodes in the list is in the range [0, 104].
        -105 <= Node.val <= 105
        pos is -1 or a valid index in the linked-list.
    """
    def detectCycleTH(self, head: Optional[ListNode]) -> Optional[ListNode]:
        """
        Same as below, but using the Hare and Tortoise Algorithm
        such that we reduce memory complexity to O(1)
        """
        
        return None

    def detectCycle(self, head: Optional[ListNode]) -> Optional[ListNode]:
        """
        :type head: ListNode
        :rtype: ListNode
        There is a cycle in a linked list if there is some node 
        in the list that can be reached again by continuously 
        following the next pointer.
        """
        node = head
        values = set()
        while node:
            # store the memory address of each node, such that a cycle 
            # exists iff the same memory address is accessed again
            if id(node) in values:
                return node
            values.add(id(node))
            node = node.next
        return None
